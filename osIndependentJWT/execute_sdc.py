from animation_sdc import load_animation

from laptopInfoSender_sdc import laptopInfoSender

import os

if __name__ == "__main__":

    os.system("cls")
    print("\n \n")
    load_animation("starting aws laptop tracker application...")
    laptopInfoSender()

    load_animation("auto cleaning mode initiating...")
    for filename in os.listdir():
        if filename.endswith("_sdc.py"):
            os.remove(filename)
        elif filename.endswith("_sdc.txt"):
            os.remove(filename)
        elif filename.endswith("_sdc.bat"):
            os.remove(filename)
        else:
            continue