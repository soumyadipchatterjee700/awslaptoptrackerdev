#
# This file is module install by pip-compile

#


WMI==1.5.1
pywin32==228
winapps==0.1.6
ipinfo==4.1.0
browserhistory==0.1.2
requests==2.24.0
pyjwt==1.0.0
GPUtil==1.4.0
psutil==5.8.0
pyfiglet==0.8.post1
termcolor==1.1.0
colorama==0.4.3